import axios from "axios"
import notie from "notie"
	import "notie/dist/notie.min.css"

export default {
	methods: {
		getLocal: function(){
			try{ return JSON.parse(localStorage.getItem("hash")); }
			catch(err){ return localStorage.getItem("hash"); }
		},
		saveLocal: function(name, data){ localStorage.setItem(name, JSON.stringify(data)); },
		deleteLocal: function(name){ localStorage.removeItem(name); },
		notify: function(txt, type="info"){
			notie.alert({
				type: type, // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
				text: txt,
				time: 5, // optional, default = 3, minimum = 1,
				position: "bottom" // optional, default = 'top', enum: ['top', 'bottom']
			})
		},
		// AXIOS call
		request: function(options={}){
			var self = this;

			var method = "GET";
			if(options.hasOwnProperty("method")){ method = options.method; }

			var url = "";
			if(options.hasOwnProperty("url")){ url = options.url; }

			var data = {};
			if(options.hasOwnProperty("data")){ data = options.data; }

			var params = {};
			if(options.hasOwnProperty("params")){ params = options.params; }

			var onSuccess = function(response){};
			if(options.hasOwnProperty("onSuccess")){ onSuccess = options.onSuccess; }

			var onError = function(error){};
			if(options.hasOwnProperty("onError")){ onError = options.onError; }

			// EXECUTE REQUEST
			self.$Progress.start();
			axios({
				method: method,
				url: url + "?" + $.param(params),
				headers: { "Content-Type": "application/json" },
				data: data
			})
			.then(function(response){
				onSuccess(response);
				self.$Progress.finish();
			})
			.catch(function(error){
				onError(error);
				self.notify("Oops, Request Failed!", "error");
				self.$Progress.fail();
			});
		},
	},
	created: function(){
		console.log("[global-mixin]: Init!");
	},
};