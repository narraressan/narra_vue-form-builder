// import components
const dashboard = () => import('./app');
const builder = () => import('../../components/form-builder/app');
import { SpringSpinner } from 'epic-spinners';

// default routes
const routes = [
	{
		name: 'dashboard',
		path: '/dashboard/:_usr',
		component: dashboard,
		props: true, // pass :_usr as a prop on this app (component)
	},
];

// vue instance
const vue = {
	props: ['_usr'],
	mixins: [],
	data() {
		return {
			loading: false,
		};
	},
	components: {
		SpringSpinner,
		builder: builder,
	},
	computed: {},
	methods: {
		logout: function() {},
	},
	mounted() {
		console.log('[dashboard]: Init!');

		console.log(this._usr);
	},
	watch: {},
};

export { routes, vue as default };
