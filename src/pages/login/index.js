// import components
const login = () => import("./app");
const loginForm = () => import("../../components/login-form/app");


// default routes
const routes = [
	{ path: "/", redirect: "/login" },
	{
		name: "login",
		path: "/login",
		component: login,
	},
]


// vue instance
const vue = {
	props: [],
	mixins: [],
	data(){
		return {}
	},
	components: {
		loginForm
	},
	computed: {},
	methods: {},
	mounted(){
		console.log("[login]: Init!");

		this.$Tracker();
	},
	watch: {},
}


export { routes, vue as default }