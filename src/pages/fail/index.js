// import components
const fail = () => import("./app");


// default routes
const routes = [
	// direct all unknown routes here
	{ path: "*", name: "fail", component: fail }
]


// vue instance
const vue = {
	props: [],
	mixins: [],
	data(){
		return {}
	},
	components: {},
	computed: {},
	methods: {},
	mounted(){
		console.log("[fail]: Init!");
	},
	watch: {},
}


export { routes, vue as default }