// extra components
import { SpringSpinner } from 'epic-spinners';

// vue instance
export default {
	name: 'Header-Template',
	props: ["text", "sub"],
	mixins: [],
	data() {
		return {
			loading: false,
		};
	},
	components: {
		SpringSpinner,
	},
	computed: {},
	methods: {},
	mounted() {
		console.log('[header-template]: Init!');
	},
	watch: {},
};
