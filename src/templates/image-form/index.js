// extra components
import { SpringSpinner } from 'epic-spinners';

// vue instance
export default {
	name: 'Image-Template',
	props: ["question", "extensions"],
	mixins: [],
	data() {
		return {
			loading: false,
		};
	},
	components: {
		SpringSpinner,
	},
	computed: {},
	methods: {},
	mounted() {
		console.log('[image-template]: Init!');
	},
	watch: {},
};
