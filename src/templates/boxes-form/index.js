// extra components
import { SpringSpinner } from 'epic-spinners';

// vue instance
export default {
	name: 'Boxes-Template',
	props: ["options", "question"],
	mixins: [],
	data() {
		return {
			loading: false,
		};
	},
	components: {
		SpringSpinner,
	},
	computed: {},
	methods: {},
	mounted() {
		console.log('[boxes-template]: Init!');
	},
	watch: {},
};
