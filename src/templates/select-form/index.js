// extra components
import { SpringSpinner } from 'epic-spinners';

// vue instance
export default {
	name: 'Select-Template',
	props: ["options", "question"],
	mixins: [],
	data() {
		return {
			loading: false,
		};
	},
	components: {
		SpringSpinner,
	},
	computed: {},
	methods: {},
	mounted() {
		console.log('[select-template]: Init!');
	},
	watch: {},
};
