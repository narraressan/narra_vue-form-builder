// extra components
import { SpringSpinner } from 'epic-spinners';

// vue instance
export default {
	name: 'Input-Template',
	props: ["placeholder", "value", "label", "required", "question"],
	mixins: [],
	data() {
		return {
			loading: false,
		};
	},
	components: {
		SpringSpinner,
	},
	computed: {},
	methods: {},
	mounted() {
		console.log('[input-template]: Init!');
	},
	watch: {},
};
