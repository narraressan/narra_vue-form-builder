// extra components
import { SpringSpinner } from 'epic-spinners';

// vue instance
export default {
	name: 'Calendar-Template',
	props: ["question", "format", "label"],
	mixins: [],
	data() {
		return {
			loading: false,
		};
	},
	components: {
		SpringSpinner,
	},
	computed: {},
	methods: {},
	mounted() {
		console.log('[calendar-template]: Init!');
	},
	watch: {},
};
