// extra components
import { SpringSpinner } from 'epic-spinners';
import draggable from 'vuedraggable';
const DynamicTemplate = () => import('../dynamic-template/app');

// template list
import formTemplates from './templates.json';

// vue instance
export default {
	name: 'Form-Builder',
	props: [],
	mixins: [],
	data() {
		return {
			// VAL
			templates: formTemplates,
			canvas: [],
			editable: {},
			// MISC
			loading: false,
		};
	},
	components: {
		draggable,
		SpringSpinner,
		DynamicTemplate,
	},
	computed: {},
	methods: {
		_download: function(){ this.notify("Feature is currently disabled."); },
		_viewJSON: function(){ this.notify("Feature is currently disabled."); },
		_preview: function(){ this.notify("Feature is currently disabled."); },
		_editable: function(args){ this.editable = this.canvas[args]; },
	},
	mounted() {
		console.log('[form-builder]: Init!');
	},
	watch: {},
};
