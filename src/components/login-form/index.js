// extra components
import { SpringSpinner } from "epic-spinners"


// vue instance
export default {
	name: "Login-Form",
	props: [],
	mixins: [],
	data(){
		return {
			email: "",
			creator: "adeanladia0129@gmail.com",
			loading: false,
		}
	},
	components: {
		SpringSpinner
	},
	computed: {},
	methods: {
		_securedLogin: function(){
			this.loading = true;
			this.$router.push("/dashboard/" + this.email);
		},
	},
	mounted(){
		console.log("[login-form]: Init!");
	},
	watch: {},
}