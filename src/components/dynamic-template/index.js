// extra components
import { SpringSpinner } from "epic-spinners"

const inputTemplate = () => import('../../templates/input-form/app')
const boxesTemplate = () => import('../../templates/boxes-form/app')
const selectTemplate = () => import('../../templates/select-form/app')
const imageTemplate = () => import('../../templates/image-form/app')
const headerTemplate = () => import('../../templates/header-form/app')
const calendarTemplate = () => import('../../templates/calendar-form/app')


// vue instance
export default {
	name: "Dynamic-Template",
	props: ["template", "_index"],
	mixins: [],
	data(){
		return {
			// MISC
			loading: false,
		}
	},
	components: {
		SpringSpinner,
	},
	computed: {
		// https://stackoverflow.com/posts/45107279/edit
		_template: function(){
			var _tmp = this.template;

			switch(true) {
				case (_tmp["_component"] == "inputTemplate"):
					_tmp["_render"] = inputTemplate;
					break;

				case (_tmp["_component"] == "boxesTemplate"):
					_tmp["_render"] = boxesTemplate;
					break;

				case (_tmp["_component"] == "selectTemplate"):
					_tmp["_render"] = selectTemplate;
					break;

				case (_tmp["_component"] == "imageTemplate"):
					_tmp["_render"] = imageTemplate;
					break;

				case (_tmp["_component"] == "headerTemplate"):
					_tmp["_render"] = headerTemplate;
					break;

				case (_tmp["_component"] == "calendarTemplate"):
					_tmp["_render"] = calendarTemplate;
					break;

				default:
					_tmp["_render"] = null;
			}

			return _tmp;
		},
	},
	methods: {
		_edit: function(editable){
			this.$emit("_editable", this._index);
		},
	},
	mounted(){
		console.log("[dynamic-template]: Init!");
	},
	watch: {},
}