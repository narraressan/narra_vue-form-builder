import Vue from "vue"
import Router from "vue-router"

// Known pages
import { routes as login_routes } from "../pages/login/index"
import { routes as dashboard_routes } from "../pages/dashboard"
import { routes as fail_routes } from "../pages/fail"

Vue.use(Router)

let routes = []
  .concat(login_routes)
  .concat(dashboard_routes)
  .concat(fail_routes)

export default new Router({ routes })
